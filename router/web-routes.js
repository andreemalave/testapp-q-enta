let router            = require('express').Router(),
    playerController  = require('../controllers/player')
 
router.get('/', (req, res, next) => { 
  res.render('admin/players/list', { title: 'Jugadores', name : 'jugadores' })
})
 
router.get('/player/:id', playerController.read, (req, res, next) => {
  res.render('admin/players/add', { title: 'Jugadores', name : 'jugadores', player : req.player })
})

module.exports = router;