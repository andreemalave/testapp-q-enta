'use strict';

const axios = require('axios'),
      fs    = require('fs')

exports.list = async (req, res) => {
    try { 


        let arr            = [],
            playersJson    = fs.readFileSync('players.json'),
            iteratePlayers = {}
            
            if(playersJson.length > 0){

                let play       = JSON.parse(playersJson).data
                iteratePlayers = play

            }else{

            let requestPlayers = await axios.get("https://www.balldontlie.io/api/v1/players"),
                players        = requestPlayers.data,
                playerObj       = players.data
                iteratePlayers  = playerObj
            }

            for (var i = 0; i < iteratePlayers.length; i++) {

            let obj               = {}
                obj.first_name    = iteratePlayers[i].first_name ? iteratePlayers[i].first_name : '--'
                obj.height_feet   = iteratePlayers[i].height_feet ? iteratePlayers[i].height_feet : '--'
                obj.height_inches = iteratePlayers[i].height_inches ? iteratePlayers[i].height_inches : '--'
                obj.last_name     = iteratePlayers[i].last_name ? iteratePlayers[i].last_name : '--'
                obj.position      = iteratePlayers[i].position ? iteratePlayers[i].position : '--'
                obj.team          = iteratePlayers[i].team.name ? iteratePlayers[i].team.name : '--'
                obj.weight_pounds = iteratePlayers[i].weight_pounds ? iteratePlayers[i].weight_pounds : '--'
                obj.id            = iteratePlayers[i].id
                arr.push(obj)

            }

            if(playersJson.length == 0){
                let data = JSON.stringify({data : arr})
                         fs.writeFileSync('players.json', data) 
            }

            res.status(200).json({data : arr})
 
        
    } catch (e) { 
        console.log('error', e)
        res.status(500).send({error : "Internal Server Error", e})
    }
}  

exports.read = async (req, res, next) => { 
    try { 

        let id             = req.params.id,
            requestPlayer  = fs.readFileSync('players.json'), 
            player         = JSON.parse(requestPlayer),
            play           = player.data  

            for(var i=0; i<play.length; i++) {
                if (play[i].id === Number(id)) {

                    let obj               = {}
                        obj.first_name    = play[i].first_name ? play[i].first_name : '--'
                        obj.height_feet   = play[i].height_feet ? play[i].height_feet : '--'
                        obj.height_inches = play[i].height_inches ? play[i].height_inches : '--'
                        obj.last_name     = play[i].last_name ? play[i].last_name : '--'
                        obj.position      = play[i].position ? play[i].position : '--'
                        obj.team          = play[i].team.name ? play[i].team.name : '--'
                        obj.weight_pounds = play[i].weight_pounds ? play[i].weight_pounds : '--'
                        obj.id            = play[i].id 

                    req.player = obj
                    next()

                }
            } 
 
    } catch (error) {
        console.log(error)
        res.status(500).send()
    }
}  

exports.update = async (req, res) => { 

    const    id = req.params.id,
        rawdata = fs.readFileSync('players.json'), 
        keys    = Object.keys(req.body),
        updates = req.body 

    try {

        const player = JSON.parse(rawdata),
              play   = player.data

        if(!player){
            res.status(404).send();
        }

        for(var i=0; i<play.length; i++) {
            if (play[i].id === Number(updates.id)) {

                keys.forEach((update) => play[i][update] = req.body[update]) 

            }
        } 

        fs.writeFileSync('players.json', JSON.stringify({data : play})) 

        res.status(200).send({message : `El jugador ha sido actualizado con exito!`})
       
    } catch (error) {
        console.log(error)
        res.status(400).send()
    }
}